// To execute this data you must install JSON package with this command
// atomsSystemUpdate
// atomsInstall json

clc
clear

while %T

    // getting content from IMGW
    [myFile1, content1] = getURL("https://danepubliczne.imgw.pl/api/data/synop/id/12560/format/json",TMPDIR)
    [myFile2, content2] = getURL("https://danepubliczne.imgw.pl/api/data/synop/id/12550/format/json",TMPDIR)
    [myFile3, content3] = getURL("https://danepubliczne.imgw.pl/api/data/synop/id/12600/format/json",TMPDIR)
    
    read1 = JSONParse(content1)
    read2 = JSONParse(content2)
    read3 = JSONParse(content3)
    
    // creating array for data
    meteo(1,:) = ["city", "date","hour", "temperature [C]", "windspeed [km/h]", "humidity [%]", "air pressure [hPa]"] 
    
    // inserting data from station in Katowice
    meteo(2,:) = [read1.stacja, read1.data_pomiaru, read1.godzina_pomiaru, read1.temperatura, read1.predkosc_wiatru, read1.wilgotnosc_wzgledna, read1.cisnienie]
    
    // inserting data from station in Częstochowa
    meteo(4,:) = ['Częstochowa', read2.data_pomiaru, read2.godzina_pomiaru, read2.temperatura, read2.predkosc_wiatru, read2.wilgotnosc_wzgledna, read2.cisnienie]
    
    // inserting data from station in Bielsko-Biała
    meteo(6,:) = ['Bielsko-Biała', read3.data_pomiaru, read3.godzina_pomiaru, read3.temperatura, read3.predkosc_wiatru, read3.wilgotnosc_wzgledna, read3.cisnienie]
    
    // creating two extra columns 
    meteo(1,8) = "Apparent temperature [C]"
    meteo(1,9) = "source"
    
    // to calculate apparent temperature in case of data from IMGW, I use Australian apparent temperature method
    
    // Katowice
    e = strtod(meteo(2,6))/100*6.105*exp(17.27*strtod(meteo(2,4))/(237.7+strtod(meteo(2,4))))
    meteo(2,8) = string(round(10*(strtod(meteo(2,4)) + 0.33*e - 0.7*strtod(meteo(2,5)) - 4))/10)
    meteo(2,9) = "IMGW"
    
    // Częstochowa
    e = strtod(meteo(4,6))/100*6.105*exp(17.27*strtod(meteo(4,4))/(237.7+strtod(meteo(4,4))))
    meteo(4,8) = string(round(10*(strtod(meteo(4,4)) + 0.33*e - 0.7*strtod(meteo(4,5)) - 4))/10)
    meteo(4,9) = "IMGW"
    
    // Bielsko-Biała
    e = strtod(meteo(6,6))/100*6.105*exp(17.27*strtod(meteo(6,4))/(237.7+strtod(meteo(6,4))))
    meteo(6,8) = string(round(10*(strtod(meteo(6,4)) + 0.33*e - 0.7*strtod(meteo(6,5)) - 4))/10)
    meteo(6,9) = "IMGW"
    
    // in case of wttr.in, apparent temperature are download direcftly from their servers, so I don't know calcuating method, which they use
    
    // getting data from wttr.in and putting them into array 
    read4 = JSONParse(mgetl(getURL('http://wttr.in/katowice?format=j1',TMPDIR)))
    
    // changing time from PM/AM to comma separated value
    time = strsplit(read4.current_condition.observation_time,[',';' '])
    if time(3) == 'PM' then converted = strtod(time(1)) + 12 end
    time24 = string(string(converted)+string('.')+string(time(2)))
    
    // writing in meteo array
    meteo(3,:) = ['Katowice', read4.weather.date(1), time24, read4.current_condition.temp_C, read4.current_condition.windspeedKmph, read4.current_condition.humidity, read4.current_condition.pressure, read4.current_condition.FeelsLikeC, 'wttr.in']
    
    // same for two other stations
    read5 = JSONParse(mgetl(getURL('http://wttr.in/czestochowa?format=j1',TMPDIR)))
    time = strsplit(read5.current_condition.observation_time,[',';' '])
    if time(3) == 'PM' then converted = strtod(time(1)) + 12 end
    time24 = string(string(converted)+string('.')+string(time(2)))
    meteo(5,:) = ['Częstochowa', read5.weather.date(1), time24, read5.current_condition.temp_C, read5.current_condition.windspeedKmph, read5.current_condition.humidity, read5.current_condition.pressure, read5.current_condition.FeelsLikeC, 'wttr.in']
    
    read6 = JSONParse(mgetl(getURL('http://wttr.in/bielsko-biala?format=j1',TMPDIR)))
    time = strsplit(read6.current_condition.observation_time,[',';' '])
    if time(3) == 'PM' then converted = strtod(time(1)) + 12 end
    time24 = string(string(converted)+string('.')+string(time(2)))
    meteo(7,:) = ['Bielsko-Biała', read6.weather.date(1), time24, read6.current_condition.temp_C, read6.current_condition.windspeedKmph, read6.current_condition.humidity, read6.current_condition.pressure, read6.current_condition.FeelsLikeC, 'wttr.in']
    
    // creating file if it doesn't exist
    x = fileinfo('weatherData.csv')
    if x == [] then x(1) = 0 end
    if x(1) == 0 then
        mopen('weatherData.csv', 'w+')
        csvWrite(meteo(1,:), 'weatherData.csv')
        mclose() 
    end 
    
    // displaying matrix in scilab console
    disp(meteo)
    
    // creating temporary file
    csvWrite(meteo(2:7,:), 'temp.csv')
    act = mgetl('temp.csv')
    
    // writing to main archive file
    filename = mopen('weatherData.csv', 'a')
    mputl(act, filename)
    mclose(filename)
    
    // reading file to search max & min values
    data = csvRead('weatherData.csv')
    
    // file is read as numbers so search max, min values
    max_stop = max(data(:,8))
    min_stop = min(data(:,8))
    
    // chceking length of file
    len = size(data)
    
    // searching for number of row in which max or min occur
    for i = 1:len(1)
        a = data(i,8)
        if max_stop == a then row_max = i end 
        if min_stop == a then row_min = i end 
    end
    
    // opening file as string
    data = csvRead('weatherData.csv',',',[],'string')
    
    // displaying max & min values
    disp('Max apparent temperature: '+data(row_max,8)+' Celcius deegres in '+data(row_max,1)+' in day '+data(row_max,2)+' at '+data(row_max,3))
    disp('Min apparent temperature: '+data(row_min,8)+' Celcius degrees in '+data(row_min,1)+' in day '+data(row_min,2)+' at '+data(row_min,3))
    
    // the same for humidity, temperature and air pressure
    data = csvRead('weatherData.csv')
    max_stop = max(data(:,4))
    min_stop = min(data(:,4))
    
    len = size(data)
    for i = 1:len(1)
        a = data(i,4)
        if max_stop == a then row_max = i end 
        if min_stop == a then row_min = i end 
    end
    
    data = csvRead('weatherData.csv',',',[],'string')
    disp('Max temperature: '+data(row_max,4)+' Celcius degrees in '+data(row_max,1)+' in day '+data(row_max,2)+' at '+data(row_max,3))
    disp('Min temperature: '+data(row_min,4)+' Celcius degrees in '+data(row_min,1)+' in day '+data(row_min,2)+' at '+data(row_min,3))
    
    data = csvRead('weatherData.csv')
    max_stop = max(data(:,4))
    min_stop = min(data(:,4))
    
    len = size(data)
    for i = 1:len(1)
        a = data(i,7)
        if max_stop == a then row_max = i end 
        if min_stop == a then row_min = i end 
    end
    
    data = csvRead('weatherData.csv',',',[],'string')
    disp('Max air pressure: '+data(row_max,7)+' hPa in '+data(row_max,1)+' in day '+data(row_max,2)+' at '+data(row_max,3))
    disp('Min air pressure: '+data(row_min,7)+' hPa in '+data(row_min,1)+' in day '+data(row_min,2)+' at '+data(row_min,3))
    
    data = csvRead('weatherData.csv')
    max_stop = max(data(:,6))
    min_stop = min(data(:,6))
    
    len = size(data)
    for i = 1:len(1)
        a = data(i,4)
        if max_stop == a then row_max = i end 
        if min_stop == a then row_min = i end 
    end
    data = csvRead('weatherData.csv',',',[],'string')
    disp('Max humidity: '+data(row_max,6)+'% in '+data(row_max,1)+' in day '+data(row_max,2)+' at '+data(row_max,3))
    disp('Min humidity: '+data(row_min,6)+'% in '+data(row_min,1)+' in day '+data(row_min,2)+' at '+data(row_min,3))

    //wait 1 hour
    sleep(3600, 's')
    
    // clear variables after all
    clear
    clc
end
